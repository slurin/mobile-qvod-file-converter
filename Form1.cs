﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
//using System.Collections.Generic;
//using System.Text.RegularExpressions;

namespace 手机快播文件合并器
{
    public partial class Form1 : Form
    {
        private delegate void UIDGs(string str);
        private System.Collections.ArrayList foundFiles = new System.Collections.ArrayList();
        private Thread scFilesThr;
        public Form1()
        {
            InitializeComponent();
        }
        private void addToList(string itemName)
        {
            if (this.InvokeRequired)
            {
                UIDGs uiu = new UIDGs(addToList);
                this.Invoke(uiu, new object[] { itemName });
            }
            else
            {
                if (itemName == "::Finish")
                {
                    label1.Text = "扫描已完成。";
                    return;
                }
                if (itemName == "::Start")
                {
                    listBox1.Items.Clear();
                    foundFiles.Clear();
                    label1.Text = "正在扫描储存…";
                    return;
                }
                string showName;
                showName=Path.GetFileName(itemName);
                showName=showName.Substring(0,showName.Length-6);
                if (showName == "")
                    showName = "UnknownFilename";
                listBox1.Items.Add(itemName.Substring(0, 1) + " >> " + showName);
                foundFiles.Add(itemName);
            }
        }
        private void scanFiles()
        {
            //string sPattern = @"_\d+\.!mv$";
            addToList("::Start");
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            foreach (DriveInfo d in allDrives)
            {
                if (d.IsReady)
                {
                    if (Directory.Exists(d.Name + "p2pcache"))
                    {
                        string[] dirs = Directory.GetDirectories(d.Name + "p2pcache");
                        foreach (string fDir in dirs)
                        {
                            string[] files = Directory.GetFiles(fDir);
                            foreach (string sFile in files)
                            {
                                if (sFile.Substring(sFile.Length - 6) == "_0.!mv")
                                {
                                    addToList(sFile);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            addToList("::Finish");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            int idx=(int)listBox1.SelectedIndex;
            //MessageBox.Show((string)foundFiles[i]);
            if (idx < 0)
            {
                MessageBox.Show("请从列表中选择一项再继续！", "无效操作");
                return;
            }
            Hide();
            Form2 frm2 = new Form2(this, (string)foundFiles[idx]);
            //frm2.
            frm2.Show();
            frm2.Activate();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            scFilesThr = new Thread(new ThreadStart(scanFiles));
            scFilesThr.Name = "Scan Files";
            scFilesThr.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (scFilesThr.IsAlive)
                scFilesThr.Abort();
            scFilesThr = new Thread(new ThreadStart(scanFiles));
            scFilesThr.Name = "Rescan Files";
            scFilesThr.Start();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            scFilesThr.Abort();
            Application.Exit();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            scFilesThr.Abort();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                string[] files = Directory.GetFiles(folderBrowserDialog1.SelectedPath);
                string foundPath = "";
                foreach (string sFile in files)
                {
                    if (sFile.Substring(sFile.Length - 6) == "_0.!mv")
                    {
                        foundPath = sFile;
                        break;
                    }
                }
                if (foundPath == "")
                {
                    MessageBox.Show("在选择的目录下未能找到所需要的文件！", "错误");
                }
                else
                {
                    Hide();
                    Form2 frm2 = new Form2(this, foundPath);
                    frm2.Show();
                    frm2.Activate();
                }
            }
        }
    }
}
