﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace 手机快播文件合并器
{
    public partial class Form2 : Form
    {
        private delegate void UDUIs(int prs);
        private Thread cbFilesThr;
        private Form fParent;
        private string workIn;
        private int totalFiles = 0;
        private FileStream ofs;
        private bool userExit = true;
        public Form2(Form parentForm,string workTask)
        {
            InitializeComponent();
            fParent = parentForm;
            workIn = workTask.Substring(0, workTask.Length - 5);
            while (File.Exists(workIn + Convert.ToString(totalFiles) + ".!mv"))
                totalFiles++;
        }
        private void updateProgress(int prs)
        {
            if (InvokeRequired)
            {
                UDUIs uiu = new UDUIs(updateProgress);
                Invoke(uiu, new object[] { prs });
            }
            else
            {
                if (prs == -1)
                {
                    MessageBox.Show("合并失败！","错误信息");
                    userExit = false;
                    Close();
                    return;
                }
                if (prs == 100)
                {
                    progressBar1.Value = 100;
                    MessageBox.Show("文件合并完成！", "完成");
                    Close();
                    return;
                }
                try { progressBar1.Value = prs; }
                catch { }
               
            }
        }
        private void mainWork()
        {
            //int mx = totalFiles;
            //FileStream ifs;
            //label1.Text = "";
            byte[] buf = new byte[8192];
            int crf=0;
            try
            {
                for (int i = 0; i < totalFiles; i++)
                {
                    FileStream ifs =new FileStream (workIn + Convert.ToString(i) + ".!mv",FileMode.Open);
                    do
                    {
                        crf = ifs.Read(buf, 0, 8192);
                        ofs.Write(buf, 0, buf.Length);
                    } while (crf != 0);
                    ifs.Close();
                    updateProgress(100 * i / totalFiles);
                }
                ofs.Close();
            }
            catch
            {
                try { updateProgress(-1); }
                catch { }
                ofs.Close();
                return;
            }
            updateProgress(100);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            //cbFilesThr.Abort();
            //fParent.Show();
            //fParent.Activate();
            Close();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //MessageBox.Show(Convert.ToString(totalFiles));
            string tmpFilename = Path.GetFileName(workIn.Substring(0, workIn.Length - 1));
            if (tmpFilename == "")
                saveFileDialog1.FileName = "UnknownFilename";
            else
                saveFileDialog1.FileName = tmpFilename;
            if (!File.Exists(workIn+"0.!mv"))
            {
                MessageBox.Show("未找到合并需要的源文件！","错误");
                Close();
                return;
            }
            cbFilesThr = new Thread(new ThreadStart(mainWork));
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //MessageBox.Show("OK");
                try
                {
                    ofs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    cbFilesThr.Name = "File Convert Main Thread";
                    cbFilesThr.Start();
                }
                catch
                {
                    MessageBox.Show("无法打开输出文件！", "错误");
                    //fParent.Show();
                    //fParent.Activate();
                    Close();
                    return;
                }
                
            }
            else
            {
                //MessageBox.Show("OK");
                //fParent.Show();
                //fParent.Activate();
                Close();
                return;
            }
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            fParent.Show();
            fParent.Activate();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            int clsQueryRes = 7;
            try
            {
                if (cbFilesThr.IsAlive && progressBar1.Value < 100 && userExit)
                {
                    clsQueryRes = (int)MessageBox.Show("文件合并未完成，您希望删除已经生成的文件吗？\n这个文件可能无法播放。", "确认操作", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (clsQueryRes == 2)
                        e.Cancel = true;
                    if (clsQueryRes == 6)
                    {
                        cbFilesThr.Abort();
                        File.Delete(saveFileDialog1.FileName);
                    }
                }
                else
                    cbFilesThr.Abort();
            }
            catch { }
        }
    }
}
